namespace Gma.UserActivityMonitorDemo
{
    partial class TestFormStatic {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestFormStatic));
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_programmeAssoc = new System.Windows.Forms.Label();
            this.label_fichier = new System.Windows.Forms.Label();
            this.checkBox_erase = new System.Windows.Forms.CheckBox();
            this.button_enregistrement = new System.Windows.Forms.Button();
            this.button_lecture = new System.Windows.Forms.Button();
            this.checkBoxKeyboard = new System.Windows.Forms.CheckBox();
            this.checkBoxMouse = new System.Windows.Forms.CheckBox();
            this.listView_actions = new System.Windows.Forms.ListView();
            this.colHeader_temps = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeader_touche = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeader_event = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeader_keyValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeader_input = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.EditBoxCol2 = new System.Windows.Forms.TextBox();
            this.EditBoxCol1 = new System.Windows.Forms.NumericUpDown();
            this.EditBoxCol3 = new System.Windows.Forms.ComboBox();
            this.openFileDialog_enreg = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog_prog = new System.Windows.Forms.OpenFileDialog();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirUnFichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectionnerUnProgrammeAssocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listView_actions_contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insererAvantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insererAprèsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.L_tempsLecture = new System.Windows.Forms.Label();
            this.L_tempsTotalLecture = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditBoxCol1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.listView_actions_contextMenu.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(12, 400);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(570, 68);
            this.textBoxLog.TabIndex = 5;
            this.textBoxLog.WordWrap = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label_programmeAssoc);
            this.groupBox2.Controls.Add(this.label_fichier);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(16, 52);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(280, 89);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Document";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Programme associé :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Fichier selectionné :";
            // 
            // label_programmeAssoc
            // 
            this.label_programmeAssoc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_programmeAssoc.Location = new System.Drawing.Point(6, 63);
            this.label_programmeAssoc.Name = "label_programmeAssoc";
            this.label_programmeAssoc.Padding = new System.Windows.Forms.Padding(2);
            this.label_programmeAssoc.Size = new System.Drawing.Size(268, 19);
            this.label_programmeAssoc.TabIndex = 16;
            this.label_programmeAssoc.Text = "Aucun programme associé";
            this.label_programmeAssoc.Click += new System.EventHandler(this.ouvertureprogrammeAssoc);
            // 
            // label_fichier
            // 
            this.label_fichier.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_fichier.Location = new System.Drawing.Point(6, 29);
            this.label_fichier.Name = "label_fichier";
            this.label_fichier.Padding = new System.Windows.Forms.Padding(2);
            this.label_fichier.Size = new System.Drawing.Size(268, 19);
            this.label_fichier.TabIndex = 13;
            this.label_fichier.Text = "Aucun fichier séléctionné";
            this.label_fichier.Click += new System.EventHandler(this.ouvertureFichier);
            // 
            // checkBox_erase
            // 
            this.checkBox_erase.AutoSize = true;
            this.checkBox_erase.Enabled = false;
            this.checkBox_erase.Location = new System.Drawing.Point(6, 44);
            this.checkBox_erase.Name = "checkBox_erase";
            this.checkBox_erase.Size = new System.Drawing.Size(138, 17);
            this.checkBox_erase.TabIndex = 14;
            this.checkBox_erase.Text = "Ecraser l\'enregistrement";
            this.checkBox_erase.UseVisualStyleBackColor = true;
            // 
            // button_enregistrement
            // 
            this.button_enregistrement.Enabled = false;
            this.button_enregistrement.Location = new System.Drawing.Point(6, 18);
            this.button_enregistrement.Name = "button_enregistrement";
            this.button_enregistrement.Size = new System.Drawing.Size(138, 20);
            this.button_enregistrement.TabIndex = 10;
            this.button_enregistrement.Text = "Continuer l\'enregistrement";
            this.button_enregistrement.UseVisualStyleBackColor = true;
            this.button_enregistrement.Click += new System.EventHandler(this.button_enregistrement_Click);
            // 
            // button_lecture
            // 
            this.button_lecture.Enabled = false;
            this.button_lecture.Location = new System.Drawing.Point(6, 19);
            this.button_lecture.Name = "button_lecture";
            this.button_lecture.Size = new System.Drawing.Size(93, 20);
            this.button_lecture.TabIndex = 9;
            this.button_lecture.Text = "Lecture";
            this.button_lecture.UseVisualStyleBackColor = true;
            this.button_lecture.Click += new System.EventHandler(this.button_lecture_Click);
            // 
            // checkBoxKeyboard
            // 
            this.checkBoxKeyboard.AutoSize = true;
            this.checkBoxKeyboard.Location = new System.Drawing.Point(6, 90);
            this.checkBoxKeyboard.Name = "checkBoxKeyboard";
            this.checkBoxKeyboard.Size = new System.Drawing.Size(58, 17);
            this.checkBoxKeyboard.TabIndex = 8;
            this.checkBoxKeyboard.Text = "Clavier";
            this.checkBoxKeyboard.UseVisualStyleBackColor = true;
            this.checkBoxKeyboard.CheckedChanged += new System.EventHandler(this.checkBoxKeyboard_CheckedChanged);
            // 
            // checkBoxMouse
            // 
            this.checkBoxMouse.AutoSize = true;
            this.checkBoxMouse.Location = new System.Drawing.Point(6, 67);
            this.checkBoxMouse.Name = "checkBoxMouse";
            this.checkBoxMouse.Size = new System.Drawing.Size(55, 17);
            this.checkBoxMouse.TabIndex = 0;
            this.checkBoxMouse.Text = "Souris";
            this.checkBoxMouse.UseVisualStyleBackColor = true;
            this.checkBoxMouse.CheckedChanged += new System.EventHandler(this.checkBoxMouse_CheckedChanged);
            // 
            // listView_actions
            // 
            this.listView_actions.AllowColumnReorder = true;
            this.listView_actions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeader_temps,
            this.colHeader_touche,
            this.colHeader_event,
            this.colHeader_keyValue,
            this.colHeader_input});
            this.listView_actions.FullRowSelect = true;
            this.listView_actions.GridLines = true;
            this.listView_actions.Location = new System.Drawing.Point(12, 204);
            this.listView_actions.MultiSelect = false;
            this.listView_actions.Name = "listView_actions";
            this.listView_actions.Size = new System.Drawing.Size(570, 164);
            this.listView_actions.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView_actions.TabIndex = 9;
            this.listView_actions.UseCompatibleStateImageBehavior = false;
            this.listView_actions.View = System.Windows.Forms.View.Details;
            this.listView_actions.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView_actions_ColumnClick);
            this.listView_actions.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.listView_actions_ColumnWidthChanged);
            this.listView_actions.SelectedIndexChanged += new System.EventHandler(this.listView_actions_SelectedIndexChanged);
            // 
            // colHeader_temps
            // 
            this.colHeader_temps.Text = "Temps";
            // 
            // colHeader_touche
            // 
            this.colHeader_touche.Text = "Touche";
            this.colHeader_touche.Width = 84;
            // 
            // colHeader_event
            // 
            this.colHeader_event.Text = "Etat";
            this.colHeader_event.Width = 85;
            // 
            // colHeader_keyValue
            // 
            this.colHeader_keyValue.Text = "Valeur touche";
            this.colHeader_keyValue.Width = 82;
            // 
            // colHeader_input
            // 
            this.colHeader_input.Text = "Entrée";
            // 
            // EditBoxCol2
            // 
            this.EditBoxCol2.Location = new System.Drawing.Point(77, 374);
            this.EditBoxCol2.Name = "EditBoxCol2";
            this.EditBoxCol2.Size = new System.Drawing.Size(72, 20);
            this.EditBoxCol2.TabIndex = 11;
            this.EditBoxCol2.TextChanged += new System.EventHandler(this.EditBoxes_TextChanged);
            // 
            // EditBoxCol1
            // 
            this.EditBoxCol1.Location = new System.Drawing.Point(12, 374);
            this.EditBoxCol1.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.EditBoxCol1.Name = "EditBoxCol1";
            this.EditBoxCol1.Size = new System.Drawing.Size(58, 20);
            this.EditBoxCol1.TabIndex = 13;
            this.EditBoxCol1.ValueChanged += new System.EventHandler(this.EditBoxes_TextChanged);
            this.EditBoxCol1.Click += new System.EventHandler(this.EditBoxes_TextChanged);
            this.EditBoxCol1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.EditBoxes_TextChanged);
            // 
            // EditBoxCol3
            // 
            this.EditBoxCol3.FormattingEnabled = true;
            this.EditBoxCol3.Items.AddRange(new object[] {
            "Appuyée",
            "Relachée"});
            this.EditBoxCol3.Location = new System.Drawing.Point(155, 374);
            this.EditBoxCol3.Name = "EditBoxCol3";
            this.EditBoxCol3.Size = new System.Drawing.Size(93, 21);
            this.EditBoxCol3.TabIndex = 14;
            this.EditBoxCol3.TextChanged += new System.EventHandler(this.EditBoxes_TextChanged);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolButton_enreg";
            this.toolStripButton1.Click += new System.EventHandler(this.button_enregFichier_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(593, 25);
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ouvrirUnFichierToolStripMenuItem,
            this.selectionnerUnProgrammeAssocToolStripMenuItem,
            this.enregistrerToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // ouvrirUnFichierToolStripMenuItem
            // 
            this.ouvrirUnFichierToolStripMenuItem.Name = "ouvrirUnFichierToolStripMenuItem";
            this.ouvrirUnFichierToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.ouvrirUnFichierToolStripMenuItem.Text = "Ouvrir un enregistrement";
            this.ouvrirUnFichierToolStripMenuItem.Click += new System.EventHandler(this.ouvertureFichier);
            // 
            // selectionnerUnProgrammeAssocToolStripMenuItem
            // 
            this.selectionnerUnProgrammeAssocToolStripMenuItem.Name = "selectionnerUnProgrammeAssocToolStripMenuItem";
            this.selectionnerUnProgrammeAssocToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.selectionnerUnProgrammeAssocToolStripMenuItem.Text = "Selectionner un programme assoc.";
            this.selectionnerUnProgrammeAssocToolStripMenuItem.Click += new System.EventHandler(this.ouvertureprogrammeAssoc);
            // 
            // enregistrerToolStripMenuItem
            // 
            this.enregistrerToolStripMenuItem.Name = "enregistrerToolStripMenuItem";
            this.enregistrerToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.enregistrerToolStripMenuItem.Text = "Enregistrer";
            this.enregistrerToolStripMenuItem.Click += new System.EventHandler(this.button_enregFichier_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(593, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxMouse);
            this.groupBox1.Controls.Add(this.button_enregistrement);
            this.groupBox1.Controls.Add(this.checkBoxKeyboard);
            this.groupBox1.Controls.Add(this.checkBox_erase);
            this.groupBox1.Location = new System.Drawing.Point(302, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(153, 119);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enregistrement";
            // 
            // listView_actions_contextMenu
            // 
            this.listView_actions_contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insererAvantToolStripMenuItem,
            this.insererAprèsToolStripMenuItem,
            this.supprimerToolStripMenuItem});
            this.listView_actions_contextMenu.Name = "listView_actions_contextMenu";
            this.listView_actions_contextMenu.Size = new System.Drawing.Size(142, 70);
            this.listView_actions_contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.listView_actions_contextMenu_Opening);
            // 
            // insererAvantToolStripMenuItem
            // 
            this.insererAvantToolStripMenuItem.Name = "insererAvantToolStripMenuItem";
            this.insererAvantToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.insererAvantToolStripMenuItem.Text = "Inserer avant";
            this.insererAvantToolStripMenuItem.Click += new System.EventHandler(this.insererAvantToolStripMenuItem_Click);
            // 
            // insererAprèsToolStripMenuItem
            // 
            this.insererAprèsToolStripMenuItem.Name = "insererAprèsToolStripMenuItem";
            this.insererAprèsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.insererAprèsToolStripMenuItem.Text = "Inserer après";
            this.insererAprèsToolStripMenuItem.Click += new System.EventHandler(this.insererAprèsToolStripMenuItem_Click);
            // 
            // supprimerToolStripMenuItem
            // 
            this.supprimerToolStripMenuItem.Name = "supprimerToolStripMenuItem";
            this.supprimerToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.supprimerToolStripMenuItem.Text = "Supprimer";
            this.supprimerToolStripMenuItem.Click += new System.EventHandler(this.supprimerToolStripMenuItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.L_tempsTotalLecture);
            this.groupBox3.Controls.Add(this.L_tempsLecture);
            this.groupBox3.Controls.Add(this.button_lecture);
            this.groupBox3.Location = new System.Drawing.Point(461, 52);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(117, 119);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lecture";
            // 
            // L_tempsLecture
            // 
            this.L_tempsLecture.Location = new System.Drawing.Point(7, 46);
            this.L_tempsLecture.Name = "L_tempsLecture";
            this.L_tempsLecture.Size = new System.Drawing.Size(50, 13);
            this.L_tempsLecture.TabIndex = 10;
            this.L_tempsLecture.Text = "0";
            this.L_tempsLecture.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // L_tempsTotalLecture
            // 
            this.L_tempsTotalLecture.Location = new System.Drawing.Point(54, 46);
            this.L_tempsTotalLecture.Name = "L_tempsTotalLecture";
            this.L_tempsTotalLecture.Size = new System.Drawing.Size(57, 13);
            this.L_tempsTotalLecture.TabIndex = 11;
            this.L_tempsTotalLecture.Text = "/0";
            this.L_tempsTotalLecture.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TestFormStatic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 480);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.EditBoxCol3);
            this.Controls.Add(this.EditBoxCol1);
            this.Controls.Add(this.EditBoxCol2);
            this.Controls.Add(this.listView_actions);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.groupBox2);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TestFormStatic";
            this.Text = "GlobalHook";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestFormStatic_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditBoxCol1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.listView_actions_contextMenu.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxKeyboard;
        private System.Windows.Forms.CheckBox checkBoxMouse;
        private System.Windows.Forms.Button button_lecture;
        private System.Windows.Forms.Button button_enregistrement;
        private System.Windows.Forms.ListView listView_actions;
        private System.Windows.Forms.ColumnHeader colHeader_temps;
        private System.Windows.Forms.ColumnHeader colHeader_touche;
        private System.Windows.Forms.ColumnHeader colHeader_event;
        private System.Windows.Forms.TextBox EditBoxCol2;
        private System.Windows.Forms.NumericUpDown EditBoxCol1;
        private System.Windows.Forms.ComboBox EditBoxCol3;
        private System.Windows.Forms.OpenFileDialog openFileDialog_enreg;
        private System.Windows.Forms.Label label_fichier;
        private System.Windows.Forms.CheckBox checkBox_erase;
        private System.Windows.Forms.ColumnHeader colHeader_keyValue;
        private System.Windows.Forms.Label label_programmeAssoc;
        private System.Windows.Forms.OpenFileDialog openFileDialog_prog;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirUnFichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistrerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ColumnHeader colHeader_input;
        private System.Windows.Forms.ToolStripMenuItem selectionnerUnProgrammeAssocToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip listView_actions_contextMenu;
        private System.Windows.Forms.ToolStripMenuItem insererAvantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insererAprèsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supprimerToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label L_tempsLecture;
        private System.Windows.Forms.Label L_tempsTotalLecture;
    }
}