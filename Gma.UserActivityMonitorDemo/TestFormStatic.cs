using System;
using System.Windows.Forms;
using Gma.UserActivityMonitor;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Gma.UserActivityMonitorDemo
{
    public partial class TestFormStatic : Form
    {
        #region DllImport fonctions
        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr windowHandle);
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);
        [DllImport("user32.dll")]
        static extern bool SetCursorPos(int X, int Y);
        #endregion

        #region Variables

            #region Mouse Flags
            private int MOUSEEVENTF_LEFTDOWN = 0x00000002;
            private int MOUSEEVENTF_LEFTUP = 0x00000004;
            private int MOUSEEVENTF_RIGHTDOWN = 0x00000008;
            private int MOUSEEVENTF_RIGHTUP = 0x00000010;
            private int MOUSEEVENTF_MIDDLEDOWN = 0x00000020;
            private int MOUSEEVENTF_MIDDLEUP = 0x00000040;
            private int MOUSEEVENTF_MOVE = 0x00000001;
            //private int MOUSEEVENTF_ABSOLUTE = 0x00008000;

            List<int> MOUSEEVENTF_DOWN = new List<int>();
            List<int> MOUSEEVENTF_UP = new List<int>();

            private string[] libellesSouris = new string[100];
            #endregion

        private DateTime tempsDepartEnreg;
        private bool recNow = false;
        private bool lectNow = false;

        private bool estEnregistre = true;

        private string programPath="";

        private ListViewColumnSorter lvwColumnSorter;

        private delegate ListView.ListViewItemCollection GetItems(ListView lstview);

        #endregion

        public TestFormStatic()
        {
            InitializeComponent();

            this.EditBoxCol2.ContextMenu = new ContextMenu();            
            this.listView_actions.ContextMenuStrip = this.listView_actions_contextMenu;

            lvwColumnSorter = new ListViewColumnSorter();
            this.listView_actions.ListViewItemSorter = lvwColumnSorter;
            adjust_editBox();

            checkBoxKeyboard.Checked = true;
            checkBoxMouse.Checked = true;

            MOUSEEVENTF_UP.Add(MOUSEEVENTF_LEFTUP);
            MOUSEEVENTF_UP.Add(MOUSEEVENTF_RIGHTUP);
            MOUSEEVENTF_UP.Add(MOUSEEVENTF_MIDDLEUP);

            MOUSEEVENTF_DOWN.Add(MOUSEEVENTF_LEFTDOWN);
            MOUSEEVENTF_DOWN.Add(MOUSEEVENTF_RIGHTDOWN);
            MOUSEEVENTF_DOWN.Add(MOUSEEVENTF_MIDDLEDOWN);

            libellesSouris[MOUSEEVENTF_LEFTDOWN] = "Clic gauche";
            libellesSouris[MOUSEEVENTF_LEFTUP] = "Clic gauche";
            libellesSouris[MOUSEEVENTF_RIGHTDOWN] = "Clic droit";
            libellesSouris[MOUSEEVENTF_RIGHTUP] = "Clic droit";
            libellesSouris[MOUSEEVENTF_MIDDLEDOWN] = "Boutton du milieu";
            libellesSouris[MOUSEEVENTF_MIDDLEUP] = "Boutton du milieu";
            libellesSouris[MOUSEEVENTF_MOVE] = "Mouvement";
            
        }

        #region Gestion des checkboxs
        private void checkBoxMouse_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxMouse.Checked)
            {
                HookManager.MouseMove += HookManager_MouseMove;
                HookManager.MouseUp += HookManager_MouseUp;
                HookManager.MouseDown += HookManager_MouseDown;
                HookManager.MouseWheel += HookManager_MouseWheel;
            }
            else
            {
                HookManager.MouseMove -= HookManager_MouseMove;
                HookManager.MouseUp -= HookManager_MouseUp;
                HookManager.MouseDown -= HookManager_MouseDown;
                HookManager.MouseWheel -= HookManager_MouseWheel;
            }
        }
        private void checkBoxKeyboard_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxKeyboard.Checked)
            {
                HookManager.KeyDown += HookManager_KeyDown;
                HookManager.KeyUp += HookManager_KeyUp;
            }
            else
            {
                HookManager.KeyDown -= HookManager_KeyDown;
                HookManager.KeyUp -= HookManager_KeyUp;
            }
        }
        #endregion

        #region Evenements clavier et souris
        private void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            keyRecord(e.KeyCode, 0);
        }
        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyValue==123)//Si F12
                button_lecture_Click(sender, e);
            keyRecord(e.KeyCode, 2);
        }
        
        private void HookManager_MouseMove(object sender, MouseEventArgs e)
        {
            mouseRecord(MOUSEEVENTF_MOVE);
        }
        private void HookManager_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                mouseRecord(MOUSEEVENTF_LEFTUP);
            if (e.Button == MouseButtons.Right)
                mouseRecord(MOUSEEVENTF_RIGHTUP);
            if (e.Button == MouseButtons.Middle)
                mouseRecord(MOUSEEVENTF_MIDDLEUP);
        }
        private void HookManager_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                mouseRecord(MOUSEEVENTF_LEFTDOWN);
            if (e.Button == MouseButtons.Right)
                mouseRecord(MOUSEEVENTF_RIGHTDOWN);
            if (e.Button == MouseButtons.Middle)
                mouseRecord(MOUSEEVENTF_MIDDLEDOWN);
        }
        private void HookManager_MouseWheel(object sender, MouseEventArgs e)
        {
            //A voir
        }
        #endregion

        #region Clics sur les boutons
        private void button_lecture_Click(object sender, EventArgs e)
        {
            if (!this.lectNow)
            {
                if (button_lecture.InvokeRequired)
                    button_lecture.BeginInvoke(((Action)(() => button_lecture.Text = "Stop")));
                else
                    this.button_lecture.Text = "Stop";

                if (button_lecture.InvokeRequired)
                    button_lecture.BeginInvoke(((Action)(() => button_enregistrement.Enabled = false)));
                else
                    button_enregistrement.Enabled = false;

                this.lectNow = true;
                
                Thread lecture = new Thread(new ThreadStart(lancement_lecture));
                lecture.Start();

            }
            else
            {
                this.lectNow = false;

                if (L_tempsLecture.InvokeRequired)
                    L_tempsLecture.BeginInvoke(((Action)(() => L_tempsLecture.Text = "0")));
                else 
                    L_tempsLecture.Text = "0";

                if(button_lecture.InvokeRequired)
                    button_lecture.BeginInvoke(((Action)(() => button_lecture.Text = "Lecture")));
                else
                    this.button_lecture.Text = "Lecture";

                if(button_lecture.InvokeRequired)
                    button_lecture.BeginInvoke(((Action)(() => button_enregistrement.Enabled = true)));
                else
                    button_enregistrement.Enabled = true;

            }

        }
        private void button_enregistrement_Click(object sender, EventArgs e)
        {
            if (!this.recNow)
            {
                this.button_enregistrement.Text = "Stop";
                this.button_lecture.Enabled = false;
                this.checkBox_erase.Focus();
                if (this.checkBox_erase.Checked)
                {
                    this.listView_actions.Items.Clear();
                    this.tempsDepartEnreg = DateTime.Now;
                }
                else
                {
                    this.tempsDepartEnreg = DateTime.Now;
                    if (listView_actions.Items.Count>0)
                        this.tempsDepartEnreg = this.tempsDepartEnreg.Add(new TimeSpan(0,0,0,0,-Convert.ToInt32(listView_actions.Items[listView_actions.Items.Count - 1].SubItems[0].Text)));
                }
                this.recNow = true;

                if (this.programPath != "")
                {
                    string[] programPathSplit = this.programPath.Split('\\');
                    string programName = programPathSplit[programPathSplit.Length - 1];

                    Process[] processes = Process.GetProcessesByName(programName.Replace(".exe", ""));
                    if (processes.Length > 0)
                        foreach (Process p in processes)
                            SetForegroundWindow(processes[0].MainWindowHandle);
                    else
                        try
                        {
                            Process.Start(this.programPath);
                        }
                        catch (Exception)
                        {
                            textBoxLog.AppendText(programName + " n'existe pas dans le r�pertoire voulu.\n");
                        }
                }

            }
            else
            {
                this.recNow = false;
                this.button_enregistrement.Text = "Continuer l'enregistrement";
                this.button_lecture.Enabled = true;

                if (listView_actions.Items.Count > 3)
                {
                    for (int i = 0; i < 4; i++)//remove 4 last records (clic on stop button)
                        listView_actions.Items.Remove(listView_actions.Items[listView_actions.Items.Count - 1]);
                }

            }
        }
        private void button_enregFichier_Click(object sender, EventArgs e)
        {
            using (System.IO.FileStream fs = System.IO.File.Create(openFileDialog_enreg.FileName)) { }

            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(openFileDialog_enreg.FileName, true))
            {
                foreach (ListViewItem actualItem in listView_actions.Items)
                {
                    if (actualItem.SubItems[4].Text == "Souris")
                    {
                        fileWriter.WriteLine("S," + actualItem.SubItems[3].Text + "," + actualItem.SubItems[2].Text + ";" + actualItem.SubItems[0].Text);
                    }
                    else if (actualItem.SubItems[4].Text == "Clavier")
                    {
                        int action = 0;
                        if (actualItem.SubItems[2].Text[0] == 'R')
                            action = 2;

                        fileWriter.WriteLine(actualItem.SubItems[3].Text + ",0," + action + ",0;" + actualItem.SubItems[0].Text);
                    }
                }
                this.estEnregistre = true;
            }

        }
        private void ouvertureprogrammeAssoc(object sender, EventArgs e)
        {
            if (openFileDialog_prog.ShowDialog().ToString() == "OK")
            {
                this.programPath = openFileDialog_prog.FileName;
                this.label_programmeAssoc.Text = openFileDialog_prog.FileName;
            }
        }
        private void ouvertureFichier(object sender, EventArgs e)
        {

            if (openFileDialog_enreg.ShowDialog().ToString() == "OK")
            {

                label_fichier.Text = openFileDialog_enreg.FileName;

                button_enregistrement.Enabled = true;
                button_lecture.Enabled = true;
                checkBox_erase.Enabled = true;

                listView_actions.Items.Clear();

                Thread ouverture = new Thread(new ThreadStart(chargementFichier));
                ouverture.Start();
                
            }
        }
        private void chargementFichier()
        {
            string[] lines = System.IO.File.ReadAllLines(openFileDialog_enreg.FileName);
            ListView.ListViewItemCollection allItems = new ListView.ListViewItemCollection(new ListView());
            string tempsTotal="0";
            int i = 0;
            foreach (string line in lines)
            {
                i++;
                string[] infos = line.Split(';');

                try
                {
                    string[] arguments = infos[0].Split(',');

                    if (arguments[0] == "S")
                    {
                        int mouseFlag = Convert.ToInt32(arguments[1]);

                        string mouseState = arguments[2];
                        if (this.MOUSEEVENTF_DOWN.Contains(mouseFlag))
                            mouseState = "Appuy�";
                        else if (this.MOUSEEVENTF_UP.Contains(mouseFlag))
                            mouseState = "Relach�";

                        string[] Items = { infos[1], libellesSouris[mouseFlag], mouseState, mouseFlag.ToString(), "Souris" };
                        allItems.Add(new ListViewItem(Items));
                    }
                    else
                    {
                        string keyState = "Relach�e";
                        if (arguments[2] == "0")
                            keyState = "Appuy�e";

                        Keys actualKey = (Keys)Convert.ToByte(arguments[0]);

                        string[] Items = { infos[1], actualKey.ToString(), keyState, ((byte)actualKey).ToString(), "Clavier" };
                        allItems.Add(new ListViewItem(Items));
                    }
                }
                catch (Exception)
                {
                    textBoxLog.BeginInvoke(((Action)(() => textBoxLog.AppendText("Fichier corrompu (ligne " + i + ")\n"))));
                }

                tempsTotal = infos[1];
            }

            L_tempsTotalLecture.BeginInvoke(((Action)(() => L_tempsTotalLecture.Text = "/" + (Convert.ToInt32(tempsTotal) / 1000))));
            listView_actions.BeginInvoke(((Action)(() => listView_actions.Items.AddRange(allItems))));
        }
        #endregion

        #region Lecture et enregistrement
        public void lancement_lecture() 
        {
            if (this.programPath != "")
            {
                string[] programPathSplit = this.programPath.Split('\\');
                string programName = programPathSplit[programPathSplit.Length - 1];

                Process[] processes = Process.GetProcessesByName(programName.Replace(".exe", ""));
                if (processes.Length > 0)
                    foreach (Process p in processes)
                        SetForegroundWindow(processes[0].MainWindowHandle);
                else
                    try
                    {
                        Process.Start(this.programPath);
                    }
                    catch (Exception)
                    {
                        textBoxLog.AppendText(programName + " n'existe pas dans le r�pertoire voulu.\n");
                    }
            }

            DateTime tempsDepartLecture = DateTime.Now;

            bool[] keyPress = new bool[262145];

            ListView.ListViewItemCollection lines = getListViewItems(listView_actions);
            foreach (ListViewItem line in lines)
            {

                if (!this.lectNow)
                    break;
                
                ListViewItem.ListViewSubItemCollection infos = line.SubItems;
                int keyTime = 0;
                byte keyValue = 0;                
                int dx=0, dy=0;

                try
                {
                    keyTime = Convert.ToInt32(infos[0].Text);
                    keyValue = Convert.ToByte(infos[3].Text);
                    if (keyValue==1)
                    { 
                        string[] mousePosition = infos[2].Text.Split('*');
                        dx = Convert.ToInt32(mousePosition[0]);
                        dy = Convert.ToInt32(mousePosition[1]);
                    }
                    
                }
                catch (Exception)
                {
                    textBoxLog.AppendText("Probl�me de conversion \n");                    
                }


                int tempsLecture = (int)((DateTime.Now - tempsDepartLecture).TotalMilliseconds);
                while (keyTime >= tempsLecture && this.lectNow)
                {
                    int tempsSleep = (keyTime - tempsLecture) > 500 ? 500 : (keyTime - tempsLecture);
                    System.Threading.Thread.Sleep(tempsSleep);
                    tempsLecture = (int)((DateTime.Now - tempsDepartLecture).TotalMilliseconds);

                    int tempsEcouleSecondes = (int)(tempsLecture / 1000);
                    L_tempsLecture.BeginInvoke(((Action)(() => L_tempsLecture.Text = tempsEcouleSecondes.ToString())));
                }

                ////////////////////SIMULATION/////////////////////
                if (this.lectNow && keyTime != 0)
                {
                    if (infos[4].Text == "Souris" && keyValue == 1)
                    {
                        Cursor.Position = new Point(dx, dy);
                    }
                    else if (infos[4].Text == "Souris")
                    {
                        mouse_event(keyValue, 0, 0, 0, 0);
                    }
                    else if (infos[4].Text == "Clavier")
                    {
                        uint keyState = 2;
                        if (infos[2].Text[0] == 'A')
                            keyState = 0;

                        keybd_event(keyValue, 0, keyState, 0);
                    }
                }

            }
            
            for (int i=0;i<keyPress.Length;i++)
            {
                try
                {
                    if (keyPress[i])
                        keybd_event((byte)i, 0, 2, 0);
                }
                catch (Exception) { }
            }

            if(this.lectNow)
                button_lecture_Click(button_lecture, EventArgs.Empty);

            if (L_tempsLecture.InvokeRequired)
                L_tempsLecture.BeginInvoke(((Action)(() => L_tempsLecture.Text = "0")));
            else
                L_tempsLecture.Text = "0";

        }

        private void keyRecord(Keys keyCode, int etatTouche)
        {
            if (this.recNow)
            {
                this.estEnregistre = false;
                TimeSpan diffrence_temps = DateTime.Now - this.tempsDepartEnreg;
                int key_time = (int)diffrence_temps.TotalMilliseconds;

                string keyState = "Relach�e";
                if (etatTouche == 0)
                    keyState = "Appuy�e";

                string[] Items = { key_time.ToString(), keyCode.ToString(), keyState, ((byte)keyCode).ToString(), "Clavier"};
                ListViewItem listItem = new ListViewItem(Items);
                listView_actions.Items.Add(listItem);

                this.listView_actions.Sort();
                listView_actions.EnsureVisible(listView_actions.Items.Count-1);
                
            }
            else if (EditBoxCol2.Focused)
            {
                listView_actions.SelectedItems[0].SubItems[1].Text = keyCode.ToString();
                listView_actions.SelectedItems[0].SubItems[3].Text = ((byte)keyCode).ToString();
                listView_actions.SelectedItems[0].SubItems[4].Text = "Clavier";
                EditBoxCol2.Text = keyCode.ToString();
                EditBoxCol2.Focus();
            }

        }
        private void mouseRecord(int mouseFlag)
        {
            Point absolutePosBox2 = PointToScreen(new Point(EditBoxCol2.Left, EditBoxCol2.Top));

            if (this.recNow && (this.MOUSEEVENTF_DOWN.Contains(mouseFlag) || this.MOUSEEVENTF_UP.Contains(mouseFlag)))
            {
                this.estEnregistre = false;
                TimeSpan diffrence_temps = DateTime.Now - this.tempsDepartEnreg;
                int key_time = (int)diffrence_temps.TotalMilliseconds;

                string mouseState = Control.MousePosition.X + "*" + Control.MousePosition.Y;

                string[] Items = { key_time.ToString(), libellesSouris[1], mouseState, "1", "Souris" };
                ListViewItem listItem = new ListViewItem(Items);
                listView_actions.Items.Add(listItem);

                if (this.MOUSEEVENTF_DOWN.Contains(mouseFlag))
                    mouseState = "Appuy�";
                if (this.MOUSEEVENTF_UP.Contains(mouseFlag))
                    mouseState = "Relach�";

                string[] Items2 = { key_time.ToString(), libellesSouris[mouseFlag], mouseState, mouseFlag.ToString(), "Souris" };
                listItem = new ListViewItem(Items2);
                listView_actions.Items.Add(listItem);

            }
            else if (Control.MousePosition.X > absolutePosBox2.X && Control.MousePosition.Y > absolutePosBox2.Y && Control.MousePosition.X < (absolutePosBox2.X + EditBoxCol2.Width) && Control.MousePosition.Y < (absolutePosBox2.Y + EditBoxCol2.Height) && mouseFlag != 1)//Si different de mouvement souris
            {

                ListViewItem.ListViewSubItemCollection subItemsFocused = listView_actions.FocusedItem.SubItems;

                subItemsFocused[1].Text = libellesSouris[mouseFlag];
                subItemsFocused[4].Text = "Souris";
                EditBoxCol2.Text = libellesSouris[mouseFlag];
                EditBoxCol2.Focus();

                majEtatSouris(mouseFlag, subItemsFocused);

            }

        }
        #endregion

        #region Changements automatiques
        private void EditBoxes_TextChanged(object sender, EventArgs e)
        {
            this.estEnregistre = false;
            if (EditBoxCol1.Focused)
                listView_actions.FocusedItem.SubItems[0].Text = EditBoxCol1.Text;
            else if (EditBoxCol2.Focused)
                listView_actions.FocusedItem.SubItems[1].Text = EditBoxCol2.Text;
            else if (EditBoxCol3.Focused)
                listView_actions.FocusedItem.SubItems[2].Text = EditBoxCol3.Text;
            else
                this.estEnregistre = true;

            try
            {
                ListViewItem.ListViewSubItemCollection subItemsFocused = listView_actions.FocusedItem.SubItems;
                majEtatSouris(Convert.ToInt32(subItemsFocused[3].Text), subItemsFocused);

                this.listView_actions.Sort();
            }
            catch (Exception)
            {

            }

        }
        private void listView_actions_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            adjust_editBox();
        }
        private void listView_actions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                EditBoxCol1.Text = listView_actions.FocusedItem.SubItems[0].Text;
            }
            catch (Exception)
            {
                EditBoxCol1.Text = "bug";
            }
            try
            {
                EditBoxCol2.Text = listView_actions.FocusedItem.SubItems[1].Text;
            }
            catch (Exception)
            {
                EditBoxCol2.Text = "bug";
            }
            try
            {
                EditBoxCol3.Text = listView_actions.FocusedItem.SubItems[2].Text;
            }
            catch (Exception)
            {
                EditBoxCol3.Text = "bug";
            }
        }
        private void adjust_editBox() 
        {
            EditBoxCol1.Width = listView_actions.Columns[0].Width;
            EditBoxCol2.Left = EditBoxCol1.Width + EditBoxCol1.Left + 2;
            
            EditBoxCol2.Width = listView_actions.Columns[1].Width;
            EditBoxCol3.Left = EditBoxCol2.Width + EditBoxCol2.Left + 2;

            EditBoxCol3.Width = listView_actions.Columns[2].Width;
        }
        private void majEtatSouris(int mouseFlag, ListViewItem.ListViewSubItemCollection subItemsFocused) 
        {
            if (subItemsFocused[2].Text[0] == 'A')//Etat appuy�
            {
                if (mouseFlag == MOUSEEVENTF_LEFTDOWN || mouseFlag == MOUSEEVENTF_LEFTUP)
                    subItemsFocused[3].Text = MOUSEEVENTF_LEFTDOWN.ToString();
                else if (mouseFlag == MOUSEEVENTF_RIGHTDOWN || mouseFlag == MOUSEEVENTF_RIGHTUP)
                    subItemsFocused[3].Text = MOUSEEVENTF_RIGHTDOWN.ToString();
                else if (mouseFlag == MOUSEEVENTF_MIDDLEDOWN || mouseFlag == MOUSEEVENTF_MIDDLEUP)
                    subItemsFocused[3].Text = MOUSEEVENTF_MIDDLEDOWN.ToString();
            }
            else//Etat relach�
            {
                if (mouseFlag == MOUSEEVENTF_LEFTDOWN || mouseFlag == MOUSEEVENTF_LEFTUP)
                    subItemsFocused[3].Text = MOUSEEVENTF_LEFTUP.ToString();
                else if (mouseFlag == MOUSEEVENTF_RIGHTDOWN || mouseFlag == MOUSEEVENTF_RIGHTUP)
                    subItemsFocused[3].Text = MOUSEEVENTF_RIGHTUP.ToString();
                else if (mouseFlag == MOUSEEVENTF_MIDDLEDOWN || mouseFlag == MOUSEEVENTF_MIDDLEUP)
                    subItemsFocused[3].Text = MOUSEEVENTF_MIDDLEUP.ToString();
            }
        }
        #endregion

        #region Divers
        private void listView_actions_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                // Inverser le sens de tri en cours pour cette colonne.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // D�finir le num�ro de colonne � trier ; par d�faut sur croissant.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Proc�der au tri avec les nouvelles options.
            this.listView_actions.Sort();
        }
        private void TestFormStatic_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.estEnregistre)
            {
                DialogResult BoxClosing = MessageBox.Show("Le fichier n'est pas enregistr�. Voulez vous l'enregitrer maintenant ?", "Fichier non enregistrer", MessageBoxButtons.YesNoCancel);
                if (BoxClosing == DialogResult.Yes)
                    button_enregFichier_Click(sender, e);
                else if (BoxClosing == DialogResult.Cancel)                   
                    e.Cancel=true;

            }

        }
        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void listView_actions_contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (listView_actions.SelectedItems[0].ToString() == "")
                e.Cancel = true;
        }

        private void insererAvantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListViewItem.ListViewSubItemCollection focusedSub=listView_actions.FocusedItem.SubItems;
            string[] Items = { focusedSub[0].Text, focusedSub[1].Text, focusedSub[2].Text, focusedSub[3].Text, focusedSub[4].Text };
            ListViewItem listItem = new ListViewItem(Items);
            listView_actions.Items.Insert(listView_actions.FocusedItem.Index, listItem);

            listView_actions.FocusedItem = listView_actions.Items[listView_actions.FocusedItem.Index-1];
            listView_actions.Items[listView_actions.FocusedItem.Index].Focused = true;
            listView_actions.Items[listView_actions.FocusedItem.Index].Selected = true;
        }
        private void insererApr�sToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListViewItem.ListViewSubItemCollection focusedSub = listView_actions.FocusedItem.SubItems;
            string[] Items = { focusedSub[0].Text, focusedSub[1].Text, focusedSub[2].Text, focusedSub[3].Text, focusedSub[4].Text };
            ListViewItem listItem = new ListViewItem(Items);
            listView_actions.Items.Insert(listView_actions.FocusedItem.Index + 1, listItem);

            listView_actions.FocusedItem = listView_actions.Items[listView_actions.FocusedItem.Index + 1];
            listView_actions.Items[listView_actions.FocusedItem.Index].Focused = true;
            listView_actions.Items[listView_actions.FocusedItem.Index].Selected = true;
        }
        private void supprimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView_actions.FocusedItem.Remove();
        }
        private ListView.ListViewItemCollection getListViewItems(ListView lstview)
        {
            ListView.ListViewItemCollection temp = new ListView.ListViewItemCollection(new ListView());
            if (!lstview.InvokeRequired)
            {
                foreach (ListViewItem item in lstview.Items)
                    temp.Add((ListViewItem)item.Clone());
                return temp;
            }
            else
                return (ListView.ListViewItemCollection)this.Invoke(new GetItems(getListViewItems), new object[] { lstview });
        }
        #endregion



    }
}